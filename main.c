#include "./linked_list.h"
#include "./higher_order_func.h"
#include "./util.h"
#include <stdint.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <inttypes.h>

static void print_space(int64_t);
static void print_new(int64_t);
static int64_t square(int64_t);
static int64_t cube(int64_t);
static int64_t summ(int64_t,int64_t);
static int64_t two(int64_t);

static void basic_test(struct LinkedList * const list);
static struct LinkedList *initial(void);
static void foreach_test(struct LinkedList * const list);
static void map_test(struct LinkedList * const list);
static void map_mut_test(struct LinkedList *list);
static void foldl_test(struct LinkedList * const list);
static struct LinkedList *iterate_test(void);
static void file_test(struct LinkedList **list);
static void file_bin_test(struct LinkedList **list);

enum status{
	OK,
	SERIALIZE_ERROR,
	DESERIALIZE_ERROR,
	SAVE_ERROR,
	LOAD_ERROR
};

int main(int argc, char* argv[]){
	struct LinkedList *list = initial();

	basic_test(list);
	
	foreach_test(list);

	map_test(list);

	map_mut_test(list);

	foldl_test(list);

	struct LinkedList *iter = iterate_test();

	file_test(&iter);

	file_bin_test(&iter);
	
	list_free(list);
	list_free(iter);
	return OK;
}

static void file_bin_test(struct LinkedList **iter){
	puts("serialize");
	if (!serialize(*iter, "./filebin.bin")){
		puts("bin file error");
		exit(SERIALIZE_ERROR);
	};
	puts("serialize complite");

	list_free(*iter);
	*iter = NULL;

	puts("deserialize");
	if (!deserialize(iter, "./filebin.bin")){
		puts("bin file error");
		exit(DESERIALIZE_ERROR);
	}
	printf("deserialize complite, list is -> ");
	foreach(*iter, print_space);
	puts("");

}

static void file_test(struct LinkedList **iter){
	puts("save in file");
	if (!save(*iter, "filename")){
		puts("fileerror");
		exit(SAVE_ERROR);
	};
	puts("saved");

	list_free(*iter);
	*iter = NULL;
	
	puts("load from file");
	if (!load(iter, "filename")){
		puts("load error");
		exit(LOAD_ERROR);
	}
	printf("load complite, list is -> ");

	foreach(*iter, print_space);
	puts("");

}

static struct LinkedList *iterate_test(void){
	struct LinkedList *iter = iterate(1, 10, two);
	puts("iteration");
	foreach(iter, print_space);
	puts("");
	return iter;
}

static void foldl_test(struct LinkedList * const list){
	int64_t su = foldl(0,list, summ);
	printf("foldl sum-> %"PRId64"\n",su);
}

static void map_mut_test(struct LinkedList * list){
	map_mut(&list, imaxabs);
	puts("map_mut abs");
	foreach(list,print_space);
	puts("");
}

static void map_test(struct LinkedList * const list){
	struct LinkedList *squares = map(list, square);
	struct LinkedList *cubes = map(list,cube);
	
	puts("map square");
	foreach(squares, print_space);
	puts("");

	puts("map cubes");
	foreach(cubes, print_space);
	puts("");
	
	list_free(squares);
	list_free(cubes);
}

static void foreach_test(struct LinkedList * const list){
	puts("foreach with spaces");
	foreach(list, print_space);
	puts("\nforeach with new line");
	foreach(list, print_new);
}

static void basic_test(struct LinkedList * const list){
	int64_t second = list_get(list, 2);
	size_t len = list_length(list);
	struct LinkedList *third = list_node_at(list, 3);
	int64_t sum = list_sum(list);
	printf("length -> %lu\nsum -> %"PRId64"\n",len,sum);
	if ((second != 0) && (third != NULL)) {
		printf("list_get second -> %"PRId64"\nlist_node_at third %"PRId64"\n",
			second,
			third->value);
	}else {
		puts("something went wrong with list_node_at or list_get");
	}
}

static struct LinkedList *initial(void){
	int64_t digit;
	struct LinkedList *list;

	printf("write your numbers(end by ^d): ");
	if (scanf("%" PRId64 "",&digit) != EOF){
		list = list_create(digit);
		while (scanf("%" PRId64 "",&digit) != EOF){
			list_add_front(&list, digit);
		}
	}
	return list;
}

static void print_space(int64_t const i){
	printf("%" PRId64 " ",i);
}

static void print_new(int64_t const i){
	printf("%" PRId64 "\n",i);
}

static int64_t square(int64_t const x){
	return x*x;
}

static int64_t cube(int64_t const x){
	return x*x*x;
}

static int64_t summ(int64_t const x, int64_t const a){
	return x+a;
}

static int64_t two(int64_t const x){
	return 2*x;
}
